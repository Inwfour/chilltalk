import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../Reducers/rootReducer';
import initialState from '../Reducers/initialState'
import logger from 'redux-logger' 

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore() {
 return createStore(
   rootReducer,
   initialState,
   composeEnhancer(applyMiddleware(thunk, logger))
 );
}