import { combineReducers } from 'redux';
import userReducer from './userReducer'
import registerReducer from './registerReduce'
import postReducer from './postReducer'
import updateuserReducer from './updateuserReducer'

export default combineReducers({
  userReducer,
  registerReducer,
  postReducer,
  updateuserReducer
});