import * as types from '../Actions/actionTypes'
import initialState from "./initialState";

export default ( state = initialState.updateuser, action ) => {
    switch ( action.type ) {
        case types.ON_UPDATE_USER_SUCCESS:
            return {
                success: action.data
            }
        case types.ON_UPDATE_USER_ERROR:
            return {
                error: action.error
            }
        default:
            return state;
    }
};
