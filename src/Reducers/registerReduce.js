import * as types from '../Actions/actionTypes'
import initialState from "./initialState";

export default ( state = initialState, action ) => {
    switch ( action.type ) {
        case types.ON_REGISTER_SUCCESS:
            return {
                user: action.data
            }
        case types.ON_REGISTER_ERROR:
            return {
                error: action.error
            }
            case types.ON_REGISTER_ERROR_CLEAR:
                return {
                  ...state,
                  error: ''
                };
        default:
            return state;
    }
};
