import * as types from "../Actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState.users, action) => {
  switch (action.type) {
    case types.ON_LOGIN:
      return {
        ...state,
        user: action.data
      };
    case types.ON_LOGIN_ERROR:
      return {
        error: action.error
      };
    case types.ON_LOGIN_ERROR_CLEAR:
      return {
        ...state,
        error: ''
      };
    case types.ON_GET_USER:
      return [
        ...action.data
      ];
    case types.ON_GET_YOUR_USER:
      return {
        ...state,
        user: action.data
      };
    case types.ON_LOGOUT_SUCCESS:
      return state
    default:
      return state;
  }
};
