import * as types from "../Actions/actionTypes";
import initialState from "./initialState";

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_ALL_POST_SUCCESS:
      return [
        ...action.data
      ];
    case types.GET_ALL_POST_ERROR:
      return {
        ...action.error
      };
    case types.GET_YOUR_POST_SUCCESS:
      if (action.data.length === 0 ) {
        return state.posts = []
      }
      return [...action.data]
    case types.GET_YOUR_POST_ERROR:
      return {
        ...action.error
      };
    case types.SAVE_YOUR_POST_SUCCESS:
      
        return [...state];
    case types.SAVE_YOUR_POST_ERROR:
      return {
        state,
        postError: 'Post Not Success'
      };
    case types.EDIT_YOUR_POST_SUCCESS:
        let post = state.map(item => {
          if(item._id == action.data._id)
          {
            return action.data
          }
          else{
            return item
          }
          
        })
        return post;
    case types.EDIT_YOUR_POST_ERROR:
      return {
        state,
        postError: 'Post Not Success'
      } 
    case types.DELETE_YOUR_POST_SUCCESS:
        return [...state];
    case types.DELETE_YOUR_POST_ERROR:
      return {
        ...state,
        error: action.error
      };
      case types.LIKE_POST_SUCCESS:
          return [...state];
      case types.LIKE_POST_ERROR:
        return {
          ...state,
          error: action.error
        };
    default:
      return state;
  }
};
