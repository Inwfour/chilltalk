import axios from 'axios'
import * as types from './actionTypes'

const loginSuccess = (data) => {
    return {
        type: types.ON_LOGIN,
        data: data
    }
}

export const onLoginError = (error) => {
    return {
        type: types.ON_LOGIN_ERROR,
        error: 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง'
    };
};

const setLocalstorage = (data) => {
    localStorage.setItem('user', JSON.stringify(data.user))
    localStorage.setItem('token', data.token)
}

export const loginClear = () => {
    return (dispatch) => {
        dispatch({
            type: types.ON_LOGIN_ERROR_CLEAR
        })
    }
}

export const registerClear = () => {
    return (dispatch) => {
        dispatch({
            type: types.ON_REGISTER_ERROR_CLEAR
        })
    }
}

export const loginUser = (user, history) => {
  return (dispatch) => {
      axios.post(`${process.env.REACT_APP_PORT}/login`, user)
      .then(response => {
          dispatch(loginSuccess(response.data));
          setLocalstorage(response.data)
          history.push('/allpost')
          window.location.reload()
      })
      .catch(() => {
          dispatch(onLoginError())
      })
  }
}

const getDataSuccess = (data) => {
    return {
        type: types.ON_GET_USER,
        data: data
    }
}

const getAllUserAxios = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${process.env.REACT_APP_PORT}/users`, { headers: { 'authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(response => {
            resolve(response.data)
        })
        .catch(error => {
            reject(error)
        })
    });
}

export const getDataUser = () => {
  return (dispatch) => {
    getAllUserAxios().then(res => {
            dispatch(getDataSuccess(res));
    })

  }
}

const getDataYourSuccess = (data) => {
    return {
        type: types.ON_GET_YOUR_USER,
        data: data
    }
}

export const getDataYourUser = () => {
  return (dispatch) => {
      var you = JSON.parse(localStorage.getItem('user'))._id
      axios.get(`${process.env.REACT_APP_PORT}/users/` + you, { headers: { 'authorization': 'Bearer ' + localStorage.getItem('token') } })
      .then(response => {
          dispatch(getDataYourSuccess(response.data));
      })
  }
}

const onRegisterSuccess = (data) => {
    return {
        type: types.ON_REGISTER_SUCCESS,
        data: data
    }
}

const onRegisterError = () => {
    return {
        type: types.ON_REGISTER_ERROR,
        error: 'สมัครไม่สำเร็จ กรุณาตรวจสอบอีกครั้ง'
    }
}

export const registerUser = (user) => {
  return (dispatch) => {
      axios.post(`${process.env.REACT_APP_PORT}/users`, user)
      .then(response => {
          dispatch(onRegisterSuccess(response.data));
      })
      .catch(error => {
        dispatch(onRegisterError());
      })
  }
}

const onUpdateUserSuccess = (data) => {
    return {
        type: types.ON_UPDATE_USER_SUCCESS,
        success: data
    }
}

const onUpdateUserError = () => {
    return {
        type: types.ON_UPDATE_USER_ERROR,
        error: 'อัพเดทไม่สำเร็จ กรุณาตรวจสอบอีกครั้ง'
    }
}

export const updateUser = (id, user) => {
  return (dispatch) => {
      axios.put(`${process.env.REACT_APP_PORT}/users/${id}`, user)
      .then(response => {
          dispatch(onUpdateUserSuccess(response.data));
      })
      .catch(error => {
        dispatch(onUpdateUserError());
      })
  }
}


const onLogoutSuccess = () => {
    return {
        type: types.ON_LOGOUT_SUCCESS,
    }
}

const logoutLocalstoreage = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
}

export const logoutUser = () => {
  return (dispatch) => {
    dispatch(onLogoutSuccess())
    logoutLocalstoreage()
  }
}