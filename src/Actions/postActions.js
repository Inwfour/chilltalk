import axios from 'axios'
import * as types from './actionTypes'

const getAllPostAxios = () => {
    return new Promise(async (resolve, reject) => {
        await axios.get(`${process.env.REACT_APP_PORT}/posts`)
        .then(async response => {
            var alluser = []
            for (let i = 0; i < response.data.length; i++) {
                await axios.get(`${process.env.REACT_APP_PORT}/users/${response.data[i].userid}`)
              .then(data => {
                  alluser.push({
                      ...response.data[i],
                      user: data.data
                  })
              })
              .catch(() => {
                  reject(getAllPostError())
              })
            }
            resolve(alluser)
        })
        .catch(() => {
            reject(getAllPostError())
        })
    });
}

export const getAllPost = () => {
  return (dispatch) => {
getAllPostAxios().then(res => {
    dispatch(getAllPostSuccess(res));
}).catch(err => {
    dispatch(err)
})
  }
}

export const getAllPostSuccess = (data) => {
  return (dispatch) => {
      dispatch({
          type: types.GET_ALL_POST_SUCCESS,
          data: data
      })
  }
}

export const getAllPostError = () => {
  return (dispatch) => {
      dispatch({
          type: types.GET_ALL_POST_ERROR,
          error: 'Get Post Error'
      })
  }
}

const getYourPostAxios = () => {
    return new Promise(async (resolve, reject) => {
        await axios.get(`${process.env.REACT_APP_PORT}/posts`)
        .then(async response => {
            var youruser = []
            var userid = (JSON.parse(localStorage.getItem('user'))._id)
            for (let i = 0; i < response.data.length; i++) {
            if (response.data[i].userid === userid) {
             await axios.get(`${process.env.REACT_APP_PORT}/users/${response.data[i].userid}`)
              .then(data => {
                youruser.push({
                      ...response.data[i],
                      user: data.data
                  })
              })
              .catch(() => {
                  reject(getYourPostError())
              })
            }
        }
        console.log('yourpost : ', youruser)
        resolve(youruser);
        })
        .catch(() => {
            reject(getYourPostError())
        })
    });
}



export const getYourPost = () => {
    return (dispatch) => {
      getYourPostAxios().then(res => {
            dispatch(getYourPostSuccess(res));
        }).catch(err => {
            dispatch(err)
        })
    }
  }
  
  export const getYourPostSuccess = (data) => {
    return (dispatch) => {
        dispatch({
            type: types.GET_YOUR_POST_SUCCESS,
            data: data
        })
    }
  }
  
  export const getYourPostError = () => {
    return (dispatch) => {
        dispatch({
            type: types.GET_YOUR_POST_ERROR,
            error: 'Get Post Error'
        })
    }
  }

  export const deleteYourPost = (id, check) => {
    return async (dispatch) => {
  axios.delete(`${process.env.REACT_APP_PORT}/posts/${id}`, { headers: { 'authorization': 'Bearer ' + localStorage.getItem('token') } }).then(async res => {
    dispatch(deletePostSuccess(res));
    if (check ===  2) {
        await getAllPostAxios().then((data) => {
            dispatch(getAllPostSuccess(data));
          })
    } else {
        await getYourPostAxios().then((data) => {
            dispatch(getYourPostSuccess(data));
          })
    }
  }).catch(err => {
      dispatch(deletePostError())
  })
    }
  }
  
  export const deletePostSuccess = (success) => {
    return (dispatch) => {
        dispatch({
            type: types.DELETE_YOUR_POST_SUCCESS,
            success: success
        })
    }
  }
  
  export const deletePostError = () => {
    return (dispatch) => {
        dispatch({
            type: types.DELETE_YOUR_POST_ERROR,
            error: 'delete not success'
        })
    }
  }


  export const saveYourPost = (data) => {
    return async (dispatch) => {
        await axios.post(`${process.env.REACT_APP_PORT}/posts`,data, { headers: { 'authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(async response => {
            getAllPostAxios().then(res => {
                dispatch(getAllPostSuccess(res));
                dispatch(saveYourPostSuccess(response.data));
            }).catch(err => {
                dispatch(err)
            })
        })
        .catch(() => {
            dispatch(saveYourPostError())
        })
    }
  }
  
  export const saveYourPostSuccess = (data) => {
    return (dispatch) => {
        dispatch({
            type: types.SAVE_YOUR_POST_SUCCESS,
            data: data
        })
    }
  }
  
  export const saveYourPostError = () => {
    return (dispatch) => {
        dispatch({
            type: types.SAVE_YOUR_POST_ERROR,
            error: 'Get Post Error'
        })
    }
  }

  export const editYourPost = (post_id,data) => {
    return async (dispatch) => {
        await axios.put(`${process.env.REACT_APP_PORT}/posts/`+post_id,data, { headers: { 'authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(async response => {
                dispatch(editYourPostSuccess(data));
        }).catch(err => {
                console.log("error");
                dispatch(editYourPostError())
        })
    }
  }
  export const editYourPostSuccess = (data) => {
    return (dispatch) => {
        dispatch({
            type: types.EDIT_YOUR_POST_SUCCESS,
            data: data
        })
    }
  }
  export const editYourPostError = () => {
    return (dispatch) => {
        dispatch({
            type: types.EDIT_YOUR_POST_ERROR,
            error: 'Edit Post Error'
        })
    }
  }


  export const likePost = (post_id,data) => {
    return async (dispatch) => {
        await axios.put(`${process.env.REACT_APP_PORT}/posts/like`, { headers: { 'authorization': 'Bearer ' + localStorage.getItem('token') } })
        .then(async response => {
                dispatch(likePostSuccess(data));
        }).catch(err => {
                dispatch(likePostError())
        })
    }
  }
  export const likePostSuccess = (data) => {
    return (dispatch) => {
        dispatch({
            type: types.LIKE_POST_SUCCESS,
            data: data
        })
    }
  }
  export const likePostError = () => {
    return (dispatch) => {
        dispatch({
            type: types.LIKE_POST_ERROR,
            error: 'Like Post Error'
        })
    }
  }