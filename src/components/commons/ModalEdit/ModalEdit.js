import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  InputGroupAddon,
  Input,
  Form,
  FormGroup,
  Col,
  Row,
  Container,
  CardImg,
  Alert
} from "reactstrap";
import { IoIosPerson, IoMdKey, IoIosMail } from "react-icons/io";
import "./ModalEdit.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { userActions } from "../../../Actions";

class ModalEdit extends Component {
  constructor(props) {
    super(props);
    this.email = React.createRef();
    this.state = {
      modal: true,
      onClickImg: null,
      profile: {
        image: "",
        email: "",
        nickname: ""
      },
      TextErrImage: "",
      isErrImage: false,
      TextErrEmail: "",
      isErrEmail: false,
      TextErrPassword: "",
      isErrPassword: false,
      TextErrNickname: "",
      isErrNickname: false
    };
    this.setState({ modal: this.props.isModalLogin });
    this.save = this.save.bind(this);
  }

  componentDidMount() {
    var splitOnClick = this.props.userdata.image.split('/')[4].split('.')[0].substring(7)
    var splitImage = this.props.userdata.image.split('/')[4]
    this.setState(prevState => ({
      ...prevState,
      onClickImg: Number(splitOnClick),
      profile: {
        ...prevState.profile,
        email: this.props.userdata.email,
        nickname: this.props.userdata.nickname,
        image: `../../../assets/icon_animal/${splitImage}`
      }
    }))
  }

  save() {
    var TextErrEmail = "";
    var isErrEmail = false;
    var TextErrNickname = "";
    var isErrNickname = false;
    var TextErrImage = "";
    var isErrImage = false;
    let flagErr = false;

    if (this.state.profile.email === "") {
      TextErrEmail = "Email required !!!";
      isErrEmail = true;
      flagErr = true;
    }

    if (this.state.profile.nickname === "") {
      TextErrNickname = "Nickname required !!!";
      isErrNickname = true;
      flagErr = true;
    }

    if (this.state.profile.image === "") {
      TextErrImage = "Please Choose Image !!!";
      isErrImage = true;
      flagErr = true;
    }

    this.setState(prevState => ({
      ...prevState,
      TextErrEmail: TextErrEmail,
      isErrEmail: isErrEmail,
      TextErrNickname: TextErrNickname,
      isErrNickname: isErrNickname,
      TextErrImage: TextErrImage,
      isErrImage: isErrImage
    }));

    if (!flagErr) {
      this.props.userActions.updateUser(this.props.userdata._id,this.state.profile);
      setTimeout(() => {
        window.location.reload()
      }, 500);
    }
  }

  onImage = i => e => {
        this.setState(prevState => ({
      ...prevState,
      onClickImg: i,
      profile: {
        ...prevState.profile,
        image: `../../../assets/icon_animal/profile${i}.png`
      }
    }));
  };

  render() {
    var a = [];
    for (let i = 1; i < 10; i++) {
      a.push(
        <CardImg
          className={`img-logo-list ${
            this.state.onClickImg === i ? "choose" : ""
          }`}
          src={require(`../../../assets/icon_animal/profile${i}.png`)}
          onClick={this.onImage(i)}
          alt="Card image cap"
        />
      );
    }
    return (
      <div>
        <Modal isOpen={this.props.isModalEdit}>
          <ModalHeader>Edit</ModalHeader>
          <Form>
            <ModalBody>
              {this.state.isErrImage && (
                <span
                  style={{
                    marginLeft: "50px",
                    color: "red",
                    fontWeight: "bold"
                  }}
                >
                  {this.state.TextErrImage}
                </span>
              )}
              <Container>
                <Row>
                  {a.map(item => (
                    <Col>{item}</Col>
                  ))}
                </Row>
              </Container>
              <br />
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <IoIosMail size={40} style={{ marginRight: "10", color: '#CE0505' }} />
                  </InputGroupAddon>
                  <Input
                    type="email"
                    value={this.state.profile.email}
                    onChange={e =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          email: e.target.value
                        }
                      })
                    }
                    placeholder="Email"
                  />
                </InputGroup>
                {this.state.isErrEmail && (
                  <span
                    style={{
                      marginLeft: "50px",
                      color: "red",
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.TextErrEmail}
                  </span>
                )}
              </FormGroup>
              <br />
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <IoIosPerson size={40} style={{ marginRight: "10", color: '#CE0505' }} />
                  </InputGroupAddon>
                  <Input
                    type="text"
                    value={this.state.profile.nickname}
                    onChange={e =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          nickname: e.target.value
                        }
                      })
                    }
                    placeholder="Nickname"
                  />
                </InputGroup>
                {this.state.isErrNickname && (
                  <span
                    style={{
                      marginLeft: "50px",
                      color: "red",
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.TextErrNickname}
                  </span>
                )}
              </FormGroup>
              {this.props.error && (
                <Alert color="danger">{this.props.error}</Alert>
              )}
              {this.props.success && (
                <Alert color="success">บันทึกสำเร็จ !!!</Alert>
              )}
            </ModalBody>
            <ModalFooter>
              <Button type="button" color="primary" onClick={this.save}>
                Register
              </Button>
              <Button
                type="button"
                color="danger"
                onClick={this.props.showModal}
              >
                Close
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.updateuserReducer.error,
    success: state.updateuserReducer.success
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ModalEdit)
);
