import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, InputGroup, InputGroupAddon, Input } from 'reactstrap';
import { IoIosPerson, IoMdKey, IoIosMail,IoIosPaper } from "react-icons/io";
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import {postActions} from '../../../Actions'
class ModalEditPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          post:null
        };
        this.toggle = this.toggle.bind(this);
        this.editPost = this.editPost.bind(this);
        
    }
    componentWillReceiveProps(nextProp)
    {
        this.setState({ 
            modal:nextProp.isModalEdit,
            post:nextProp.post 
        },()=> {console.log(this.state);})
    }
    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }),()=>{
            this.props.onblur(this.state.modal)
        });
    }
    onChange(event)
    {
        const { name, value } = event.target;
        if(name == 'post')
        {
            this.setState( prevState => ( {
                ...prevState,
               post:{
                   ...prevState.post,
                   post:value
               }
            } ) );
        }
        else
        {
            this.setState( prevState => ( {
                ...prevState,
                [ name ]: value
            } ) );
        }
       
    }
    editPost(){
        this.props.postActions.editYourPost(this.state.post._id,this.state.post)
        this.toggle()
    }
    render() {
        return (
        <div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size={this.props.size}>
            <ModalHeader toggle={this.toggle}>Edit Post</ModalHeader>
            <ModalBody>
            <InputGroup>
            <InputGroupAddon addonType="prepend"><IoIosPaper size={40} style={{ marginRight: '10' }}/></InputGroupAddon>
            <Input className="style-input" value={this.state.post?this.state.post.post:''} onChange={(e) => this.onChange(e)} placeholder="" type="textarea" maxLength='50' rows={3} name="post" id="post" />
        </InputGroup>
        <br />
        {/* <InputGroup>
            <InputGroupAddon addonType="prepend"><IoIosPerson size={40} style={{ marginRight: '10' }} /></InputGroupAddon>
            <Input type="text" placeholder="Nickname" />
        </InputGroup> */}
            </ModalBody>
            <ModalFooter>
            <Button color="primary" onClick={this.editPost}>Save</Button>
            <Button color="danger" onClick={this.toggle}>Close</Button>
            </ModalFooter>
        </Modal>
        </div>
        )
    }
}
const mapStateToProps = state => {
    return {}
}
const mapDispatchToProps = dispatch => {
   return { 
    postActions: bindActionCreators(postActions, dispatch)
    }
 }

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(ModalEditPost));
