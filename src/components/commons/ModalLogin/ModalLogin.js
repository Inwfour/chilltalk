import {bindActionCreators} from 'redux'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, InputGroup, InputGroupAddon, Input, Alert } from 'reactstrap';
import { connect } from 'react-redux';
import { IoIosPerson, IoMdKey, IoIosMail } from "react-icons/io";
import React, { Component } from 'react'
import { withRouter } from "react-router-dom";

import {userActions} from '../../../Actions'

class ModalLogin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
    }
  }

  closeModal = () => {
    this.props.showModal()
  }

  signIn = () => {
      var user = {
        email: this.state.email,
        password: this.state.password
      }
      this.props.userActions.loginUser(user, this.props.history)
  }

  componentDidMount() {
    this.props.userActions.loginClear()
  }


  render() {
    if (localStorage.getItem('token') !== null) {
      this.props.showModal()
    }
    return (
      <div>
      <Modal isOpen={this.props.isModalLogin}>
        <ModalHeader>Login</ModalHeader>
        { this.props.error &&       ( <Alert color="danger">
        { this.props.error }
        
      </Alert> )}
        <ModalBody>
        <InputGroup>
        <InputGroupAddon addonType="prepend"><IoIosMail size={40} style={{ marginRight: '10' }}/></InputGroupAddon>
        <Input value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} type="email" placeholder="Email" />
      </InputGroup>
      <br />
      <InputGroup>
        <InputGroupAddon addonType="prepend"><IoMdKey size={40} style={{ marginRight: '10' }} /></InputGroupAddon>
        <Input value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} type="password" placeholder="Password" />
      </InputGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={this.signIn}>Sign In</Button>
          <Button color="danger" onClick={this.closeModal}>Close</Button>
        </ModalFooter>
      </Modal>
    </div>
    )
  }
}

const mapStateToProps = state => {
  return { 
    error: state.userReducer.error,
    user: state.userReducer.user
   }
}

const mapDispatchToProps = dispatch => {
   return { 
    userActions: bindActionCreators(userActions, dispatch)
    }
 }

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(ModalLogin));
