import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  InputGroupAddon,
  Input,
  Form,
  FormGroup,
  Col,
  Row,
  Container,
  CardImg,
  Alert
} from "reactstrap";
import { IoIosPerson, IoMdKey, IoIosMail } from "react-icons/io";
import "./ModalRegister.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { userActions } from "../../../Actions";

class ModalRegister extends Component {
  constructor(props) {
    super(props);
    this.email = React.createRef();
    this.state = {
      modal: true,
      onClickImg: null,
      profile: {
        image: "",
        email: "",
        password: "",
        nickname: ""
      },
      TextErrImage: "",
      isErrImage: false,
      TextErrEmail: "",
      isErrEmail: false,
      TextErrPassword: "",
      isErrPassword: false,
      TextErrNickname: "",
      isErrNickname: false
    };
    this.setState({ modal: this.props.isModalLogin });
    this.save = this.save.bind(this);
  }

  componentDidMount() {
    this.props.userActions.registerClear();
  }

  save() {
    var TextErrEmail = "";
    var isErrEmail = false;
    var TextErrPassword = "";
    var isErrPassword = false;
    var TextErrNickname = "";
    var isErrNickname = false;
    var TextErrImage = "";
    var isErrImage = false;
    let flagErr = false;

    if (this.state.profile.email === "") {
      TextErrEmail = "Email required !!!";
      isErrEmail = true;
      flagErr = true;
    }

    if (this.state.profile.password === "") {
      TextErrPassword = "Password required !!!";
      isErrPassword = true;
      flagErr = true;
    }

    if (this.state.profile.nickname === "") {
      TextErrNickname = "Nickname required !!!";
      isErrNickname = true;
      flagErr = true;
    } else if (this.state.profile.nickname.length > 7) {
      TextErrNickname = "Nickname < 8 Charecter !!!";
      isErrNickname = true;
      flagErr = true;
    }

    if (this.state.profile.image === "") {
      TextErrImage = "Please Choose Image !!!";
      isErrImage = true;
      flagErr = true;
    }

    this.setState(prevState => ({
      ...prevState,
      TextErrEmail: TextErrEmail,
      isErrEmail: isErrEmail,
      TextErrPassword: TextErrPassword,
      isErrPassword: isErrPassword,
      TextErrNickname: TextErrNickname,
      isErrNickname: isErrNickname,
      TextErrImage: TextErrImage,
      isErrImage: isErrImage
    }));

    if (!flagErr) {
      this.props.userActions.registerUser(this.state.profile);
      this.setState(prevState => ({
        ...prevState,
        onClickImg: null,
        profile: {
          ...prevState.profile,
          image: '',
          email: '',
          password: '',
          nickname: '',
        }
      }))
    }
  }

  onImage = i => e => {
    this.setState(prevState => ({
      ...prevState,
      onClickImg: i,
      profile: {
        ...prevState.profile,
        image: `../../../assets/icon_animal/profile${i}.png`
      }
    }));
  };

  render() {
    var a = [];
    for (let i = 1; i < 10; i++) {
      a.push(
        <CardImg
          className={`img-logo-list ${
            this.state.onClickImg === i ? "choose" : ""
          }`}
          src={require(`../../../assets/icon_animal/profile${i}.png`)}
          onClick={this.onImage(i)}
          alt="Card image cap"
        />
      );
    }
    return (
      <div>
        <Modal isOpen={this.props.isModalRegister}>
          <ModalHeader>Register</ModalHeader>
          <Form>
            <ModalBody>
              {this.state.isErrImage && (
                <span
                  style={{
                    marginLeft: "50px",
                    color: "red",
                    fontWeight: "bold"
                  }}
                >
                  {this.state.TextErrImage}
                </span>
              )}
              <Container>
                <Row>
                  {a.map(item => (
                    <Col>{item}</Col>
                  ))}
                </Row>
              </Container>
              <br />
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <IoIosMail size={40} style={{ marginRight: "10", color: '#CE0505' }} />
                  </InputGroupAddon>
                  <Input
                    type="email"
                    value={this.state.profile.email}
                    onChange={e =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          email: e.target.value
                        }
                      })
                    }
                    placeholder="Email"
                  />
                </InputGroup>
                {this.state.isErrEmail && (
                  <span
                    style={{
                      marginLeft: "50px",
                      color: "red",
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.TextErrEmail}
                  </span>
                )}
              </FormGroup>
              <br />
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <IoMdKey size={40} style={{ marginRight: "10", color: '#CE0505' }} />
                  </InputGroupAddon>
                  <Input
                    type="password"
                    value={this.state.profile.password}
                    onChange={e =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          password: e.target.value
                        }
                      })
                    }
                    placeholder="Password"
                  />
                </InputGroup>
                {this.state.isErrPassword && (
                  <span
                    style={{
                      marginLeft: "50px",
                      color: "red",
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.TextErrPassword}
                  </span>
                )}
              </FormGroup>
              <br />
              <FormGroup>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <IoIosPerson size={40} style={{ marginRight: "10", color: '#CE0505' }} />
                  </InputGroupAddon>
                  <Input
                    type="text"
                    value={this.state.profile.nickname}
                    onChange={e =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          nickname: e.target.value
                        }
                      })
                    }
                    placeholder="Nickname"
                  />
                </InputGroup>
                {this.state.isErrNickname && (
                  <span
                    style={{
                      marginLeft: "50px",
                      color: "red",
                      fontWeight: "bold"
                    }}
                  >
                    {this.state.TextErrNickname}
                  </span>
                )}
              </FormGroup>
              {this.props.error && (
                <Alert color="danger">{this.props.error}</Alert>
              )}
              {this.props.user.message && (
                <Alert color="success">บันทึกสำเร็จ !!!</Alert>
              )}
            </ModalBody>
            <ModalFooter>
              <Button type="button" color="primary" onClick={this.save}>
                Register
              </Button>
              <Button
                type="button"
                color="danger"
                onClick={this.props.showModal}
              >
                Close
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.registerReducer.error,
    user: state.registerReducer.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ModalRegister)
);
