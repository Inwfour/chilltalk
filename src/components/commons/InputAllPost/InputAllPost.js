import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import './inputAllPost.css'

export default class InputAllPost extends Component {
  constructor(props) {
    super(props)
    this.state = {
      textPost: ""
    }
  }
  onChange(e) {
this.setState({
  textPost: e.target.value
})
  }

  save = () => {
this.props.onSavePost(this.state.textPost)
this.setState({ textPost: '' })
  }
  render() {
    return (
      <div>
        <Form>
        <FormGroup>
          <Label className="input-title" for="exampleText">เขียนข้อความของคุณ</Label>
          <Input className="style-input" value={this.state.textPost} onChange={(e) => this.onChange(e)} placeholder="คุณกำลังคิดอะไรอยู่ ..." type="textarea" maxLength='50' rows={2} name="post" id="post" />
        </FormGroup>
          <Button type="button" onClick={this.save} color="primary" size="lg">โพสท์ข้อความ</Button>
        </Form>
      </div>
    );
  }
}
