import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { IoIosHeartEmpty, IoIosHeart } from "react-icons/io";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { postActions } from "../../../Actions";
import socketIOClient from 'socket.io-client'
import moment from 'moment'
import {
  Card,
  CardBody,
  Button,
  CardTitle,
  CardText,
  CardImg,
  CardFooter,
  Container, Row, Col
} from "reactstrap";
import './CardList.css'
import Fade from 'react-reveal/Fade';
import {ModalEditPost} from '../ModalEditPost';
class CardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalEdit: false,
      changeLike: false,
      message: [],
      endpoint: "http://localhost:5000/"  
    };
    this.openModal = this.openModal.bind(this);
    this.onblur = this.onblur.bind(this);
  }
  openModal(){
    this.setState({
      modalEdit:true
    })
  }
  onblur(status){
    this.setState({
      modalEdit:status
    })
  }

onLike = () => {
  this.setState(prevState => ({
    ...prevState,
    changeLike: !this.state.changeLike
  }))
  const { endpoint } = this.state
  const socket = socketIOClient(endpoint)
  socket.emit('sent-message', 'สำเร็จ')
  // this.props.postActions.likePost()
}

// response = () => {
//   const { endpoint, message } = this.state
//   const temp = message
//   const socket = socketIOClient(endpoint)
//   socket.on('new-message', (messageNew) => {
//     console.log('messageNew : ', messageNew)
//     temp.push(messageNew)
//     this.setState({ message: temp })
//   })
// }

// componentDidMount () {
//   this.response()
// }

  render() {
    console.log('image : ', this.props.posts.user.image)
    return (
        <Col xs="12" md="6" lg="4">
                <Fade bottom>
        <Card className="top">
          <div className="profile-list">
          <CardImg
            className="img-logo-list"
            src={this.props.posts.user.image}
            alt="Card image cap"
          />
          <div className="between">
          <p className="title-profile-list">{this.props.posts.user.nickname}</p>
              <p className="text-muted">{moment(this.props.posts.updatedAt).fromNow()}</p>
              </div>
          </div>
          <CardBody>
            <CardText className="post-detail-text">
              {this.props.posts.post}
            </CardText>
            {
          this.state.message.map((data, i) =>
            <div key={i} >
              {i + 1} : {data}
            </div>
          )
        }
          </CardBody>
          <CardFooter>
            {
              this.state.changeLike ? (
                <IoIosHeart onClick={this.onLike} color="red" size="40" style={{ marginLeft: '10', marginRight: '10', cursor: 'pointer' }} />
              ) : (
                <IoIosHeartEmpty onClick={this.onLike} size="40" style={{ marginLeft: '10', marginRight: '10', cursor: 'pointer' }} />
              )
            }
            <span style={{ fontWeight: 'bold' }}>{this.props.posts.like.length > 0 ? this.props.posts.like.filter(item => item.status === true).length : 0} Likes</span>
            {
              localStorage.getItem('user') ? 
                this.props.posts.userid === (JSON.parse(localStorage.getItem('user')))._id ? (
                  <>
                  <Button onClick={() => this.props.deletePost(this.props.posts._id)} className="button-action" color="danger">Delete</Button>
                  <Button className="button-action" color="warning" onClick={() => this.openModal()}>Edit</Button>
                  </>
                ) : (
                  <></>
                ) : (
                  <></>
                )
              
            }
          </CardFooter>
        </Card>
        </Fade>
        <ModalEditPost size="lg" isModalEdit={this.state.modalEdit} post={this.props.posts} onblur={this.onblur} />
        </Col>
    );
  }
}

const mapStateToProps = state => {
  return {
    //
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postActions: bindActionCreators(postActions, dispatch)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CardList)
);
