import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { InputAllPost } from "../../commons/InputAllPost";
import { CardList } from "../../commons/CardList";
import { postActions } from "../../../Actions";
import { bindActionCreators } from "redux";
import "./YourPost.css";
import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { connect } from "react-redux";
import { Row } from "reactstrap";

class YourPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.posts.length >= 0) {
      return {
        posts: props.posts
      };
    }
    return null;
  }

  deletePost = id => {
    confirmAlert({
      title: "คุณต้องการลบโพสท์ ?",
      message: "คุณต้องการลบโพสท์ : " + id + " หรือไม่ ?",
      buttons: [
        {
          label: "Yes",
          onClick: () => this.props.postActions.deleteYourPost(id, 1)
        },
        {
          label: "No"
        }
      ]
    });
  };

  componentDidMount() {
    this.props.postActions.getYourPost();
  }
  render() {
    return (
      <div className="list">
        <h2 className="title-profile-list">โพสท์ของคุณ</h2>
        <hr />
        <div>
          {
            this.state.posts.length > 0 ? (
              <Row>
              {this.state.posts.map((item, index) => (
                <CardList
                  keys={index}
                  posts={item}
                  deletePost={this.deletePost}
                />
              ))}s
            </Row>
            ) : (
              <div className="notfoundpost">
               <h1 className="notfoundpost-h1">ไม่มีโพสท์ของคุณ</h1>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.postReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postActions: bindActionCreators(postActions, dispatch)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(YourPost)
);
