import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { userActions } from "../../../Actions"
import { ModalEdit } from "../../commons/ModalEdit";
import {
  CardImg,
  Button
} from "reactstrap";
import "./Profile.css";

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {},
      isModalEdit: false,
    }
  }

  openModalEdit = () => {
    this.setState(prevState => ({
      ...prevState,
      isModalEdit: !this.state.isModalEdit
    }));
  }

  
  static getDerivedStateFromProps( props, state ) {
    if ( props.user) {
        return {
            user: props.user
        };
    }
    return null;
}

  componentDidMount() {
    this.props.userActions.getDataYourUser()
  }

  render() {
    return (
      <div className="parent-div">
          <CardImg
          className="img-profile"
            src={`${this.state.user.image}`}
            alt="Profile Logo"
          />
          <p>Email : {`${this.state.user.email}`}</p>
          <p>Nickname : {`${this.state.user.nickname}`}</p>
          <Button onClick={() => this.openModalEdit()} color="success" size="lg" round>Edit</Button>
          {this.state.isModalEdit && (
            <ModalEdit
              showModal={this.openModalEdit}
              isModalEdit={this.state.isModalEdit}
              userdata={this.state.user}
            />
            )}
            
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Profile)
);
