import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { IoIosLogIn, IoIosCreate } from "react-icons/io";
import React, { Component } from "react";
import { withRouter, NavLink } from "react-router-dom";

import { userActions } from "../../../Actions"
import { ModalEdit } from "../../commons/ModalEdit";
import { ModalLogin } from "../../commons/ModalLogin";
import { ModalRegister } from "../../commons/ModalRegister";

import "./NavPost.css";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  CardImg
} from "reactstrap";

class NavPost extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      isModalLogin: false,
      isModalRegister: false,
      user: {}
    };
  }

  static getDerivedStateFromProps( props, state ) {
    if ( props.user) {
        return {
            user: props.user
        };
    }
    return null;
}

  componentDidMount() {
    if (localStorage.getItem('token') !== null ) {
      this.props.userActions.getDataYourUser()
    }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  openModalLogin = () => {
    this.setState(prevState => ({
      ...prevState,
      isModalLogin: !this.state.isModalLogin
    }));
  };

  openModalRegister = () => {
    this.setState(prevState => ({
      ...prevState,
      isModalRegister: !this.state.isModalRegister
    }));
  }

  logout = () => {
    this.props.userActions.logoutUser()
  }

  render() {
    return (
      <div>
        <Navbar className="navbar" light expand="lg" fixed={`top`}>
          <CardImg
            className="card-img hovicon"
            src={require("../../../assets/logo_chilltalk.png")}
            alt="CHILLTALK"
          />
          <NavbarBrand className="title" to="/">
            CHILLTALK
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" style={{ marginRight: "30px" }} navbar>
              <NavItem style={{ margin: 'auto' }}>
                <NavLink
                  activeClassName="clickNavLink"
                  className="nombore"
                  to="/allpost"
                >
                  ALL POSTS <span style={{ color: "gray" }}>|</span>
                  <span className="slider"></span>
                </NavLink>
                <NavLink
                  activeClassName="clickNavLink"
                  className="nombore"
                  to="/yourpost"
                >
                  YOUR POSTS <span style={{ color: "gray" }}>|</span>
                  <span className="slider"></span>
                </NavLink>
                <NavLink
                  activeClassName="clickNavLink"
                  className="nombore"
                  to="/profile"
                >
                  PROFILE
                  {/* { Object.keys(this.props.data.getDataReducer).length } */}
                  <span className="slider"></span>
                </NavLink>
              </NavItem>
              {localStorage.getItem("token") == null ? (
                <NavItem>
                  <p
                    style={{ cursor: "pointer" }}
                    className="nombore"
                    onClick={this.openModalLogin}
                  >
                    <IoIosLogIn style={{ marginRight: "10" }} size="50" />
                    Login |<span className="slider"></span>
                  </p>
                  <p
                    style={{ cursor: "pointer" }}
                    className="nombore"
                    onClick={this.openModalRegister}
                  >
                    <IoIosCreate style={{ marginRight: "10" }} size="50" />
                    Register
                    <span className="slider"></span>
                  </p>
                </NavItem>
              ) : (
                <div>
                <div class="block">
                  <NavItem>
                  <NavLink
                      activeClassName="clickNavLink"
                      className="nombore"
                      to="/profile"
                    >
                      {
                        this.state.user && (
                          <CardImg
                          className="card-profile"
                          src={this.state.user.image}
                          alt="Image Profile"
                        />
                        )
                      }
                      { this.state.user.nickname }
                    </NavLink>
                  </NavItem>
                </div>
                <div class="block">
                <NavItem className="nombore" onClick={this.logout}>
                <NavLink
                style={{ marginLeft: '10px' }}
                      activeClassName="clickNavLink"
                      to="/profile"
                    >
                      ออกจากระบบ
                      </NavLink>
                  </NavItem>
                </div>
                </div>
              )}
            </Nav>
            {this.state.isModalLogin && (
              <ModalLogin
                showModal={this.openModalLogin}
                isModalLogin={this.state.isModalLogin}
              />
            )}
                        {this.state.isModalRegister && (
            <ModalRegister
              showModal={this.openModalRegister}
              isModalRegister={this.state.isModalRegister}
            />
            )}
            {/* <ModalEdit/> */}
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(NavPost)
);
