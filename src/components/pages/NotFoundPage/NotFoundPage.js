import React, { Component } from 'react'

export default class NotFoundPage extends Component {
  render() {
    return (
      <div className="parent-div">
        <img width="60%" src={ require('../../../assets/404.jpg') }></img>
      </div>
    )
  }
}
