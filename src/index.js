import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import * as userActions from './Actions/userActions'
import * as postActions from './Actions/postActions'
import { BrowserRouter } from 'react-router-dom'
import configureStore from './store/store.js';
import { Provider } from 'react-redux'

var store = configureStore()

store.dispatch( userActions.getDataUser() )
// store.dispatch( postActions.getAllPost() )

// if (localStorage.getItem('token') !== null) {
// store.dispatch( userActions.getDataYourUser() )
// }

const AppWithRouter = () => (
  <Provider store={configureStore()}>
  <BrowserRouter>
        <App/>
  </BrowserRouter>
  </Provider>
)

ReactDOM.render(<AppWithRouter />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
