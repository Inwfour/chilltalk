import React, { Component } from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'
import './App.css'
import {
  NavPost,
  AllPost,
  YourPost,
  Profile,
  NotFoundPage
} from './components/pages'
import PrivateRoute from "./components/hoc/PrivateRoute/PrivateRoute";

export const Routes = {

  register: "/register",
  allPost: "/allpost",
  yourPost: "/yourpost",
  profile: "/profile",
  notFound: "/notfound",

};

class App extends Component {
  render() {
    return (
      <div>
      <NavPost/>
      <div className="container hight">
        <Switch>
        <Route path={ Routes.allPost } exact component={ AllPost } />
        <PrivateRoute exact path={ Routes.yourPost } component={ YourPost } />
        <PrivateRoute exact path={ Routes.profile } component={ Profile } />
        <Redirect exact from="/" to={ Routes.allPost } />
        <Route component={NotFoundPage} />
        </Switch>
      </div>
      </div>
    )
  }
}

export default App